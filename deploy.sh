#/bin/bash

# TODO: 
# * Check if there is a more useful way to force recreation:
#   https://stackoverflow.com/questions/32612650/how-to-get-docker-compose-to-always-re-create-containers-from-fresh-images


force=${1:-0}
if [[ "$force" == "force" ]]; then
    docker stack rm bf
    /bin/sleep 20
fi

# Make sure we are working with the most recent version of bookface
git pull

# Run command
docker stack deploy -c docker-compose.yml bf
